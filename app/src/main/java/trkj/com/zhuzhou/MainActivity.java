package trkj.com.zhuzhou;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

public class MainActivity extends FragmentActivity {
    private FragmentTabHost tabHost;
    private String[] titles = {"新闻", "爆料", "活动", "我的"};
    private String[] titlesEn = {"News", "Tipoff", "Activity", "Mine"};
    private Class[] fragments = {
            NewsFragment.class,
            TipoffFragment.class,
            ActivityFragment.class,
            MineFragment.class
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getActionBar().hide();

        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(), R.id.real_content);
        for (int i = 0; i < titles.length; i++) {
            TabHost.TabSpec tabSpec = tabHost.newTabSpec(titles[i]);
            tabSpec.setIndicator(changeTabHostColor(i));
            tabHost.addTab(tabSpec, fragments[i], null);
        }
        tabHost.getTabWidget().setDividerDrawable(null);
    }

    public View changeTabHostColor(int position) {
        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        View root = inflater.inflate(R.layout.item_tabhost, null);

        TextView tvTitle = (TextView) root.findViewById(R.id.title);
        TextView tvTitleEn = (TextView) root.findViewById(R.id.titleEn);

        tvTitle.setText(titles[position]);
        tvTitleEn.setText(titlesEn[position]);

        return root;
    }
}
