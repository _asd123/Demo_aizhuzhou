package trkj.com.zhuzhou;

import android.app.Application;

import org.xutils.x;

/**
 * Created by Administrator on 2016/4/24.
 */
public class InitXutils extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        x.Ext.init(this);
        x.Ext.setDebug(true);
    }
}
