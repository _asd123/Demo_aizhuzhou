package trkj.com.zhuzhou;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Administrator on 2016/4/23.
 */
public class AllTipoffFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_alltipoff, null);
        TextView textView = (TextView) root.findViewById(R.id.tv);
        Bundle b = getArguments();
        textView.setText(b.getString("content"));
        return root;
    }
}
