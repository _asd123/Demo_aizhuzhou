package trkj.com.zhuzhou;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;


/**
 * Created by Administrator on 2016/4/23.
 */
public class ActivityFragment extends Fragment {
    private GridView gridView;
    private ListView listView;
    private String[] list = new String[40];
    private String[] areas = {"天元区", "芦淞区", "荷塘区", "石峰区",
            "株洲县", "攸县", "炎陵县", "茶陵县", "醴陵市", "云龙示范区"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_activity, null);
        gridView = (GridView) root.findViewById(R.id.gv_area);
        listView = (ListView) root.findViewById(R.id.lv_activity);

        gridView.setAdapter(new GridViewAdapter());
        listView.setAdapter(new ListViewAdapter());

        return root;
    }


    //gridview适配器
    class GridViewAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return areas.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View root = inflater.inflate(R.layout.item_gridview_txt, null);
            TextView tvArea = (TextView) root.findViewById(R.id.text);
            tvArea.setText(areas[i]);

            return root;
        }
    }

    //listview的适配器
    class ListViewAdapter extends BaseAdapter {
        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public int getItemViewType(int position) {
            if (position % 3 == 0) {
                return 0;
            } else if (position % 3 == 1) {
                return 1;
            } else {
                return 2;
            }
        }

        @Override
        public int getCount() {
            return list.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View root = null;
            int type = getItemViewType(i);
            if (type == 0) {
                root = inflater.inflate(R.layout.item_listview_activity1, null);
            } else if (type == 1) {
                root = inflater.inflate(R.layout.item_listview_activity2, null);
            } else {
                root = inflater.inflate(R.layout.item_listview_activity3, null);
            }
            return root;
        }
    }
}
