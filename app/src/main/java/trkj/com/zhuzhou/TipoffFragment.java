package trkj.com.zhuzhou;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.ScrollIndicatorView;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;

import org.xutils.common.util.DensityUtil;

/**
 * Created by Administrator on 2016/4/23.
 */
public class TipoffFragment extends Fragment {
    private String[] titles = {"全部", "已处理", "我的爆料"};
    private ViewPager vpTipoff;
    private ScrollIndicatorView indicatorView;
    private IndicatorViewPager indicatorViewPager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tipoff, null);
        vpTipoff = (ViewPager) root.findViewById(R.id.vp_tipoff);
        indicatorView = (ScrollIndicatorView) root.findViewById(R.id.vp_indicator);

        indicatorView.setScrollBar(new ColorBar(getActivity(), Color.RED, 4));
        indicatorView.setOnTransitionListener(new OnTransitionTextListener().setColorId(getActivity(), R.color.red, R.color.gray));
        indicatorViewPager = new IndicatorViewPager(indicatorView, vpTipoff);
        indicatorViewPager.setAdapter(new TabPageIndicatorAdapter(getChildFragmentManager()));

        return root;
    }

    class TabPageIndicatorAdapter extends IndicatorViewPager.IndicatorFragmentPagerAdapter {

        public TabPageIndicatorAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return titles.length;
        }


        @Override
        public View getViewForTab(int i, View view, ViewGroup viewGroup) {
            TextView tvLabel = new TextView(getActivity());
            tvLabel.setText(titles[i]);
            tvLabel.setPadding(DensityUtil.getScreenWidth() / 9, 20, 20, 20);
            tvLabel.setTextSize(17);
            return tvLabel;
        }

        @Override
        public Fragment getFragmentForPage(int i) {
            Fragment fragment = new AllTipoffFragment();
            Bundle bundle = new Bundle();
            bundle.putString("content", titles[i]);
            fragment.setArguments(bundle);
            return fragment;
        }
    }
}
