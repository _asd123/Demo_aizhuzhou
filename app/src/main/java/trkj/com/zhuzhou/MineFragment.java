package trkj.com.zhuzhou;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Administrator on 2016/4/23.
 */
public class MineFragment extends Fragment {
    private GridView gridView;
    private ImageView ivImg;
    private TextView tvTitle;
    private String[] titles = {"关注", "粉丝", "特别关注", "收藏", "评论", "爆料", "消息", "同城活动", "设置"};
    private int[] img = {
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection,
            R.mipmap.collection};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mine, null);
        gridView = (GridView) root.findViewById(R.id.gv_mine);
        gridView.setAdapter(new ItemAdapter());

        return root;
    }

    public class ItemAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View root = inflater.inflate(R.layout.item_gridview_icon, null);

            ivImg = (ImageView) root.findViewById(R.id.img);
            tvTitle = (TextView) root.findViewById(R.id.title);

            ivImg.setImageResource(img[i]);
            tvTitle.setText(titles[i]);
            return root;
        }
    }
}
