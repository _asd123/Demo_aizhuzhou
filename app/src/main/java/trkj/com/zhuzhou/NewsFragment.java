package trkj.com.zhuzhou;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.ScrollIndicatorView;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;


/**
 * Created by Administrator on 2016/4/23.
 */
public class NewsFragment extends Fragment {
    private String[] labels = {"头条", "热点", "娱乐", "体育", "株洲", "财经"};
    private ViewPager vpNews;
    private ScrollIndicatorView indicatorView;
    private IndicatorViewPager indicatorViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_news, null);
        vpNews = (ViewPager) root.findViewById(R.id.vp_news);
        indicatorView = (ScrollIndicatorView) root.findViewById(R.id.vp_indicator);

        indicatorView.setScrollBar(new ColorBar(getActivity(), Color.RED, 4));
        indicatorView.setOnTransitionListener(new OnTransitionTextListener().setColorId(getActivity(), R.color.red, R.color.gray));
        indicatorViewPager = new IndicatorViewPager(indicatorView, vpNews);
        indicatorViewPager.setAdapter(new TabPageIndicatorAdapter(getChildFragmentManager()));

        return root;
    }

    class TabPageIndicatorAdapter extends IndicatorViewPager.IndicatorFragmentPagerAdapter {

        public TabPageIndicatorAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return labels.length;
        }


        @Override
        public View getViewForTab(int i, View view, ViewGroup viewGroup) {
            TextView tvLabel = new TextView(getActivity());
            tvLabel.setText(labels[i]);
            tvLabel.setPadding(55, 20, 30, 20);
            tvLabel.setTextSize(15);
            return tvLabel;
        }

        @Override
        public Fragment getFragmentForPage(int i) {
            Fragment fragment = new HotNewsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("content", labels[i]);
            fragment.setArguments(bundle);
            return fragment;
        }
    }

}
