package trkj.com.zhuzhou;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Administrator on 2016/4/27.
 */
public class HotNewsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_hotnews, null);
        TextView tv = (TextView) root.findViewById(R.id.tv);
        Bundle bundle = getArguments();
        tv.setText(bundle.getString("content"));
        return root;
    }
}
